# zsh-aliases


```
#Git
alias gco="git checkout"
alias gcm="git commit -m"

## These could also be done as git aliases, it would probably be cleaner that way
alias gpo="git pull origin"
alias gpom="git pull origin master"
alias gpoma="git pull origin main"
alias gpod="git pull origin dev"

alias gpma="git push origin main"
alias gpm="git push origin master"


alias gb="git branch"
alias gs="git status"
alias gap="git add -p"

#Npx
alias npe="npx eslint"
alias npw="npx prettier --write"

#Sites
alias sites="cd ~/sites"

```
